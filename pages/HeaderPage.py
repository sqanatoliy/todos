from locators import Header
from .BasePage import BasePage
import allure
from time import sleep


class HeaderPage(BasePage):

    @allure.step
    def add_new_item(self, item: str):
        self._wait_for_clickable(Header.new_item_field)
        self._input(Header.new_item_field, item)
        sleep(2)
        self._press_enter(Header.new_item_field)
        return HeaderPage(self._driver)

    @allure.step
    def add_many_new_items(self, items: list):
        for item in items:
            self._wait_for_clickable(Header.new_item_field)
            self._input(Header.new_item_field, item)
            sleep(2)
            self._press_enter(Header.new_item_field)
        return HeaderPage(self._driver)



