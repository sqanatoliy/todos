from locators import Footer
from .BasePage import BasePage
import allure


class FooterPage(BasePage):

    @allure.step
    def select_all_items(self):
        self._wait_for_clickable(Footer.items_all)
        self._click(Footer.items_all)
        return FooterPage(self._driver)

    @allure.step
    def select_all_active(self):
        self._wait_for_clickable(Footer.items_active)
        self._click(Footer.items_active)
        return FooterPage(self._driver)

    @allure.step
    def select_all_completed(self):
        self._wait_for_clickable(Footer.items_complete)
        self._click(Footer.items_complete)
        return FooterPage(self._driver)

    @allure.step
    def select_clear_completed(self):
        self._wait_for_clickable(Footer.clear_completed)
        self._click(Footer.clear_completed)
        return FooterPage(self._driver)

    @allure.step
    def verify_qty_items_left(self, expected_qty: int):
        self._wait_for_visible(Footer.items_left)
        actual_qty = self._get_element_text(Footer.items_left)
        assert int(actual_qty) == expected_qty,\
            f"The expected quantity of items left does not match the actual quantity," \
            f" the actual quantity of items left is: '{actual_qty}', expected quantity is: '{expected_qty}'"
        return FooterPage(self._driver)
