from locators import Main
from .BasePage import BasePage
import allure
from time import sleep


class MainPage(BasePage):

    @allure.step
    def toggle_all_items(self):
        self._wait_for_clickable(Main.toggle_all)
        self._click(Main.toggle_all)
        return MainPage(self._driver)

    @allure.step
    def toggle_item(self, index=0):
        self._wait_for_clickable_elements(Main.toggle, index=index)
        self._click_elements(Main.toggle, index=index)
        sleep(2)
        return MainPage(self._driver)

    @allure.step
    def edit_item_name(self, new_item_name, index=0):
        self._wait_for_clickable_elements(Main.item_label, index=index)
        self._left_db_click(Main.item_label, index=index)
        self._input_elements(Main.edit_field, new_item_name, index=index)
        sleep(2)
        self._press_elements_enter(Main.edit_field, index=index)
        return MainPage(self._driver)

    @allure.step
    def delete_item(self, index=0):
        self._wait_for_clickable_elements(Main.toggle, index=index)
        self._hover_elements(Main.toggle, index=index)
        self._wait_for_clickable_elements(Main.destroy_button, index=index)
        self._click_elements(Main.destroy_button, index=index)
        return MainPage(self._driver)

    @allure.step
    def verify_name_item(self, expected_name_item, index=0):
        self._wait_for_visible_elements(Main.item_label, index=index)
        actual_name_item = self._get_element_text_elements(Main.item_label, index=index)
        assert actual_name_item == expected_name_item,\
            f"The expected element name does not match the actual one, the expected name is: '{expected_name_item}'," \
            f" the actual name is: '{actual_name_item}'"
        return MainPage(self._driver)

    @allure.step
    def verify_items_toggled(self, name_attr, expected_value):
        qty_items = range(len(self._all_elements(Main.item_row)))
        for item in qty_items:
            value_attribute = self._get_attribute_values(Main.item_row, name_attr, index=item)
            assert value_attribute == expected_value,\
                f"The actual value of the attribute: '{name_attr}' not as expected." \
                f" Expected value is '{expected_value}', " \
                f"actual value attribute is: '{value_attribute}'"
        return MainPage(self._driver)

    @allure.step
    def verify_item_not_displayed(self):
        if len(self._all_elements(Main.item_label)) > 0:
            element = self._get_element_text_elements(Main.item_label)
            assert len(self._all_elements(Main.item_label)) == 0, \
                f"No element is expected to be represented, but item '{element}' is"
        return MainPage(self._driver)

    @allure.step
    def verify_expected_qty_all_items_displayed(self, expected_qty_items: int):
        qty_items_displayed = len(self._all_elements(Main.item_label))
        assert expected_qty_items == qty_items_displayed,\
            f"An unexpected qty of items is displayed. Expected qty: '{expected_qty_items}'," \
            f" but displayed: '{qty_items_displayed}'"
        return MainPage(self._driver)

    @allure.step
    def verify_expected_qty_active_items_displayed(self, expected_active_items: int):
        active_elements = 0
        all_elements = len(self._all_elements(Main.item_row))
        for element in range(all_elements):
            if self._get_attribute_values(Main.item_row, 'class', index=element) == 'active':
                active_elements += 1
        assert active_elements == expected_active_items,\
            f'The qty of active elements does not match the expected.' \
            f' Expected qty: "{expected_active_items}". Actual qty active elements: "{active_elements}"'

    @allure.step
    def verify_expected_qty_completed_items_displayed(self, expected_completed_items: int):
        completed_elements = 0
        all_elements = len(self._all_elements(Main.item_row))
        for element in range(all_elements):
            if self._get_attribute_values(Main.item_row, 'class', index=element) == 'completed':
                completed_elements += 1
        assert completed_elements == expected_completed_items,\
            f'The qty of completed elements does not match the expected.' \
            f' Expected qty: "{expected_completed_items}". Actual qty completed elements: "{completed_elements}"'
