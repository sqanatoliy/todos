from .BasePage import BasePage
from .HeaderPage import HeaderPage
from .MainPage import MainPage
from .FooterPage import FooterPage
from pages.helpers.Helpers import Helpers
