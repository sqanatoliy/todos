from pages import HeaderPage, FooterPage, MainPage
from pages.helpers.Helpers import Helpers
import allure

items = Helpers.items


@allure.epic("The case of toggling items")
class TestToggleItem:
    @allure.description('Toggle all items')
    def test_toggle_all_items(self, driver):
        HeaderPage(driver) \
            .add_many_new_items(items)
        MainPage(driver) \
            .verify_name_item(items[0], index=0)
        FooterPage(driver) \
            .verify_qty_items_left(4)
        MainPage(driver) \
            .toggle_all_items() \
            .verify_items_toggled('class', 'completed')

    @allure.description('Toggle one item')
    def test_toggle_one_item(self, driver):
        HeaderPage(driver) \
            .add_new_item(items[-1])
        MainPage(driver) \
            .verify_name_item(items[-1], index=0)
        FooterPage(driver) \
            .verify_qty_items_left(1)
        MainPage(driver) \
            .toggle_item() \
            .verify_items_toggled('class', 'completed')


@allure.epic("The case of editing item")
class TestEditItem:
    @allure.description('Edit name item')
    def test_edit_name_item(self, driver):
        base_part = items[0]
        additional_part = ' with vitamin D3'
        HeaderPage(driver) \
            .add_new_item(base_part)
        MainPage(driver) \
            .edit_item_name(additional_part) \
            .verify_name_item(base_part+additional_part)


@allure.epic("The case of deleting item")
class TestDeleteItem:
    @allure.description('Delete item')
    def test_delete_item(self, driver):
        HeaderPage(driver) \
            .add_new_item(items[0])
        MainPage(driver) \
            .delete_item() \
            .verify_item_not_displayed()
