from pages import HeaderPage, FooterPage, MainPage
from pages.helpers.Helpers import Helpers
import allure

items = Helpers.items


@allure.epic("The case of adding items")
class TestAddItems:
    @allure.description('Add one item')
    def test_add_one_item(self, driver):
        name_item = "Some name for test"
        HeaderPage(driver) \
            .add_new_item(name_item)
        MainPage(driver) \
            .verify_name_item(name_item)
        FooterPage(driver) \
            .verify_qty_items_left(1)

    @allure.description('Add many items')
    def test_add_many_items(self, driver):
        HeaderPage(driver) \
            .add_many_new_items(items)
        MainPage(driver) \
            .verify_name_item(items[1], index=1)
        FooterPage(driver) \
            .verify_qty_items_left(4)
