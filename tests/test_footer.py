from pages import HeaderPage, FooterPage, MainPage
from pages.helpers.Helpers import Helpers
import allure
import pytest

items = Helpers.items


@allure.epic("The case of selecting category All")
class TestAllItems:
    @allure.description('The selected category is All')
    def test_select_all_items(self, driver):
        HeaderPage(driver) \
            .add_many_new_items(items)
        MainPage(driver) \
            .verify_name_item(items[1], index=1) \
            .toggle_item(2)
        FooterPage(driver) \
            .verify_qty_items_left(3) \
            .select_all_completed() \
            .select_all_items()
        MainPage(driver) \
            .verify_expected_qty_all_items_displayed(4)


@allure.epic("The case of selecting category Active")
class TestActiveItems:
    @allure.description('The selected category is Active')
    def test_select_active_items(self, driver):
        HeaderPage(driver) \
            .add_many_new_items(items)
        MainPage(driver) \
            .verify_name_item(items[1], index=1) \
            .toggle_item(1) \
            .toggle_item(2)
        FooterPage(driver) \
            .verify_qty_items_left(2) \
            .select_all_active()
        MainPage(driver) \
            .verify_expected_qty_active_items_displayed(2)

    @allure.description('The selected category is Active without an active item')
    def test_select_active_items_without_active_item(self, driver):
        HeaderPage(driver) \
            .add_many_new_items(items)
        MainPage(driver) \
            .verify_name_item(items[1], index=1) \
            .toggle_item(1) \
            .toggle_item(2) \
            .toggle_item(0) \
            .toggle_item(3)
        FooterPage(driver) \
            .verify_qty_items_left(0) \
            .select_all_active()
        MainPage(driver) \
            .verify_expected_qty_active_items_displayed(0)


@allure.epic("The case of selecting category Completed")
class TestCompletedItems:
    @allure.description('The selected category is Completed')
    def test_select_completed_items(self, driver):
        HeaderPage(driver) \
            .add_many_new_items(items)
        MainPage(driver) \
            .verify_name_item(items[1], index=1) \
            .toggle_item(0) \
            .toggle_item(1) \
            .toggle_item(2)
        FooterPage(driver) \
            .verify_qty_items_left(1) \
            .select_all_completed()
        MainPage(driver) \
            .verify_expected_qty_completed_items_displayed(3)

    @allure.description('The selected category is Completed without an completed element')
    def test_select_completed_items_without_completed_item(self, driver):
        HeaderPage(driver) \
            .add_many_new_items(items)
        MainPage(driver) \
            .verify_name_item(items[1], index=1)
        FooterPage(driver) \
            .verify_qty_items_left(4) \
            .select_all_completed()
        MainPage(driver) \
            .verify_expected_qty_completed_items_displayed(0)


@allure.epic("The case of selecting category Clear Completed")
class TestClearCompletedItems:
    @allure.description('The selected category is Clear Completed')
    def test_select_clear_completed_items(self, driver):
        HeaderPage(driver) \
            .add_many_new_items(items)
        MainPage(driver) \
            .verify_name_item(items[1], index=1) \
            .toggle_item(0) \
            .toggle_item(1) \
            .toggle_item(2)
        FooterPage(driver) \
            .verify_qty_items_left(1) \
            .select_clear_completed()
        MainPage(driver) \
            .verify_expected_qty_all_items_displayed(1)

    @allure.description('This test will be fail')
    def test_select_clear_completed_items_fail(self, driver):
        HeaderPage(driver) \
            .add_many_new_items(items)
        MainPage(driver) \
            .verify_name_item(items[1], index=1) \
            .toggle_item(0) \
            .toggle_item(1) \
            .toggle_item(2) \
            .toggle_item(3)
        FooterPage(driver) \
            .verify_qty_items_left(0) \
            .select_clear_completed()
        MainPage(driver) \
            .verify_expected_qty_all_items_displayed(1)

    @pytest.mark.xfail
    @allure.description('This test will be mark fail')
    def test_select_clear_completed_items_mark_fail(self, driver):
        HeaderPage(driver) \
            .add_many_new_items(items)
        MainPage(driver) \
            .verify_name_item(items[1], index=1) \
            .toggle_item(0) \
            .toggle_item(1) \
            .toggle_item(2) \
            .toggle_item(3)
        FooterPage(driver) \
            .verify_qty_items_left(0) \
            .select_clear_completed()
        MainPage(driver) \
            .verify_expected_qty_all_items_displayed(1)
