class Main:
    toggle_all = {'css': '#toggle-all'}
    toggle = {'css': '#todo-list > li > div > input'}
    item_row = {'css': '#todo-list > li'}
    item_label = {'css': '#todo-list > li > div > label'}
    edit_field = {'css': '#todo-list > li > input'}
    destroy_button = {'css': '#todo-list > li > div > button'}
