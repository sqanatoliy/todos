class Footer:
    items_left = {'css': '#todo-count > strong'}
    items_all = {'css': '#filters > li:nth-child(1) > a'}
    items_active = {'css': '#filters > li:nth-child(2) > a'}
    items_complete = {'css': '#filters > li:nth-child(3) > a'}
    clear_completed = {'css': '#clear-completed'}
